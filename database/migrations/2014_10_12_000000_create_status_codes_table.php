<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateStatusCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_codes', function (Blueprint $table) {
            $table->id('status_id');
            $table->string('status_code');
            $table->integer('status');
        });

        DB::table('status_codes')->insert([
            ['status_code' => 'Administrator', 'status' => 1],
            ['status_code' => 'Not Verified', 'status' => 1],
            ['status_code' => 'Verified', 'status' => 1],
            ['status_code' => 'Processes', 'status' => 1],
            ['status_code' => 'Sent', 'status' => 1],
            ['status_code' => 'Delivered', 'status' => 1],
            ['status_code' => 'Cancel', 'status' => 1]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_codes');
    }
}
