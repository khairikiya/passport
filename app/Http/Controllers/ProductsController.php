<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Products;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class ProductsController extends Controller
{
    public function index(): \Illuminate\Http\JsonResponse
    {
        $data = Products::getDataProduk();
        if (!$data) {
            return response()->json(['message' => 'success', 'data' => $data], 204);
        } else {
            return response()->json(['message' => 'success', 'data' => $data], 200);
        }
    }

    public function store(Request $request)
    {

        //validasi inputan dari depan
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'user_id' => 'required',
        ]);

        //ketika validasi gagal
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        DB::beginTransaction();

        $dataProduk = new Products();
        $dataProduk->name = $request->name;
        $dataProduk->price = $request->price;
        $dataProduk->user_id = $request->user_id;

        $dataProduk->save();

        DB::commit();
        return response()->json(['message' => 'Add Product Success'], 200);
    }

    public function detail($id)
    {
        $product = Products::getDataDetail($id);

        if ($product) {
            return response()->json(['message' => 'success', 'data' => $product], 200);
        } else {
            return response()->json(['message' => 'success', 'data' => $product], 204);
        }
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'user_id' => 'required',
        ]);

        //ketika validasi gagal
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        DB::beginTransaction();

        $dataProduk = array(
            'name' => $request->name,
            'price' => $request->price,
            'user_id' => $request->user_id
        );

        $update = Products::where('id', $id)->update($dataProduk);

        DB::commit();
        return response()->json(['message' => 'Update Product Success'], 200);
    }

    public function delete(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $update = Products::where('id', $request->product_id)
                ->update([
                    'status' =>  0
                ]);
        } catch (\Exception $e) {

            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 200);
        }

        DB::commit();
        return response()->json(['message' => 'success'], 200);
    }
}
