<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;


class PassportController extends Controller
{
    public function register(Request $request): \Illuminate\Http\JsonResponse
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $resorce       = $request->file('image');
        $name   = $resorce->getClientOriginalName();
        $newName = date('ysm') .  str_replace(" ", "", $name);
        $resorce->move(\base_path() . "/public/assets/user-img/", $newName);


        $digits = 4;
        $otp_code =  str_pad(rand(1, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);

        DB::beginTransaction();
        try {

            $dataUser = new User();
            $dataUser->name = $request->name;
            $dataUser->email = $request->email;
            $dataUser->image = 'assets/user-img/' . $newName;
            $dataUser->role_id = 2;
            $dataUser->otp_code = $otp_code;
            $dataUser->status_id = 2;
            $dataUser->status = 1;
            $dataUser->password = bcrypt($request->password);
            $dataUser->save();

            $dataUser->createToken('nApp')->accessToken;
        } catch (\Exception $e) {
            //Error Message for register process
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 400);
        }

        $details = [
            'body' => $otp_code
        ];

        Mail::to($request->email)->send(new \App\Mail\OtpMail($details));

        //Registration Success
        DB::commit();
        return response()->json([
            'message' => 'register success',
            'email' => $request->email
        ], 200);
    }

    public function konfirmasiOtp(Request $request)
    {
        $email = $request->email;
        $otp = $request->otp_code;

        $dataUser = User::getDataUserByEmail($email);

        if ($dataUser) {
            if ($dataUser->otp_code == $otp) {
                DB::beginTransaction();

                $update = User::where('email', $email)->update(array('status_id' => 3));
                if ($update) {
                    DB::commit();
                    return response()->json([
                        'message' => 'Berhasil Memverifikasi Akun'
                    ], 200);
                }
            } else {
                return response()->json([
                    'message' => 'Kode OTP Salah'
                ], 406);
            }
        } else {
            return response()->json([
                'message' => 'Email tidak ditemukan'
            ], 404);
        }
    }

    public function kirimOtp(Request $request)
    {
        $email = $request->email;
        $digits = 4;
        $otp_code =  str_pad(rand(1, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT);
        DB::beginTransaction();
        $update = User::where('email', $email)->update(array('otp_code' => $otp_code));

        if ($update) {
            DB::commit();
            $details = [
                'body' => $otp_code
            ];

            Mail::to($request->email)->send(new \App\Mail\OtpMail($details));
            return response()->json([
                'message' => 'Kode OTP berhasil dikirim ke email ' . $email,
                'email' => $email
            ], 200);
        } {
            return response()->json([
                'message' => 'Email tidak ditemukan'
            ], 404);
        }
    }


    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $cek = User::where([
            ['email', '=', $request->email],
            ['role_id', '=', '2']
        ])->first();
        if ($cek != null) {
            if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                $user = Auth::user();
                $userModel = new User();
                $userData = $userModel->cekUserVerified($user->id);

                if ($userData) {

                    $userData->token = $user->createToken('nApp')->accessToken;
                    return response()->json($userData, 200);
                } else {

                    return response()->json([
                        'message' => 'Akun akun belum diverifikasi',
                        'email' => $request->email
                    ], 406);
                }
            } else {
                return response()->json(['error' => 'Password salah'], 203);
            }
        } else {
            return response()->json(['error' => 'User tidak ditemukan'], 202);
        }
    }

    public function registerAdmin(Request $request): \Illuminate\Http\JsonResponse
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $resorce       = $request->file('image');
        $name   = $resorce->getClientOriginalName();
        $newName = date('ysm') .  str_replace(" ", "", $name);
        $resorce->move(\base_path() . "/public/assets/user-img/", $newName);

        DB::beginTransaction();
        try {

            $dataUser = new User();
            $dataUser->name = $request->name;
            $dataUser->email = $request->email;
            $dataUser->image = 'assets/user-img/' . $newName;
            $dataUser->role_id = 1;
            $dataUser->status_id = 1;
            $dataUser->status = 1;
            $dataUser->password = bcrypt($request->password);
            $dataUser->save();

            $dataUser->createToken('nApp')->accessToken;
        } catch (\Exception $e) {
            //Error Message for register process
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 400);
        }

        //Registration Success
        DB::commit();
        return response()->json([
            'message' => 'Register Admin Success',
            'email' => $request->email
        ], 200);
    }

    public function loginAdmin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }
        $cek = User::where([
            ['email', '=', $request->email],
            ['role_id', '=', '1']
        ])->first();
        if ($cek != null) {
            if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                $user = Auth::user();

                $dataAdmin = User::where('email', $request->email)->first();
                $dataAdmin->token = $user->createToken('nApp')->accessToken;
                return response()->json($dataAdmin, 200);
            } else {
                return response()->json(['error' => 'Password salah'], 203);
            }
        } else {
            return response()->json(['error' => 'User tidak ditemukan'], 202);
        }
    }
}
