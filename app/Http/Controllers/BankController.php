<?php



namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;


class BankController extends Controller
{

    public function index(Request $request)
    {
        $id = $request->user_id;

        $dataBank = DB::table('banks')->where('user_id', $id)->get();


        if (!$dataBank) {
            return response()->json(['message' => 'success', 'data' => $dataBank], 204);
        } else {
            return response()->json(['message' => 'success', 'data' => $dataBank], 200);
        }
    }
    public function store(Request $request)
    {
        //Request API Validation
        $validator = Validator::make($request->all(), [
            'user_id' => 'required',
            'nama_bank' => 'required',
            'nomor_rekening' => 'required',
        ]);

        //Check if API Request invalid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        //Start transaction register
        DB::beginTransaction();
        try {

            //Insert Data user
            $dataBank = new Bank();
            $dataBank->user_id = $request->user_id;
            $dataBank->nama_bank = $request->nama_bank;
            $dataBank->nomor_rekening = $request->nomor_rekening;
            $dataBank->save();
        } catch (\Exception $e) {
            //Error Message for register process
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 200);
        }
        //Add Bank Success
        DB::commit();
        return response()->json(['message' => 'Add Bank success'], 200);
    }

    public function detail(Request $request, $id)
    {
        $id = $request->bank_id;

        $dataBank = Bank::getDataBankById($id);

        if (!$dataBank) {
            return response()->json(['message'  => 'success', 'data' => $dataBank], 204);
        } else {
            return response()->json(['message'  => 'success', 'data' => $dataBank], 200);
        }
    }

    function update(Request $request, $id)
    {
        //Request API Validation
        $validator = Validator::make($request->all(), [
            'nama_bank' => 'required',
            'nomor_rekening' => 'required',
        ]);

        //Check if API Request invalid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        //Start transaction register
        DB::beginTransaction();
        try {

            //update data bank

            $dataBank = array(
                'nama_bank' => $request->nama_bank,
                'nomor_rekening' => $request->nomor_rekening
            );
            $update = Bank::where('id', $id)->update($dataBank);
        } catch (\Exception $e) {
            //Error Message for register process
            DB::rollBack();
            return response()->json(['message' => $e->getMessage()], 200);
        }
        //Add Bank Success
        DB::commit();
        return response()->json(['message' => 'Edit Bank success'], 200);
    }

    public function delete($id)
    {
        $dataBank = array(
            'status' => 0
        );
        $update = Bank::where('id', $id)->update($dataBank);
        return response()->json(['message' => 'Delete Bank success'], 200);
    }
}
