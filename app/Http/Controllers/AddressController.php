<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\User_address;
use Illuminate\Support\Facades\Validator;

class AddressController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $user_id = Auth::user()->id;
        $dataAddress = User_address::getAlamatByUserId($user_id);

        if (count($dataAddress) > 0) {
            return response()->json($dataAddress, 200);
        } else {
            return response()->json(['message' => 'Alamat Kosong'], 204);
        }
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'postal_code' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $userAddress = new User_address();
        $user_id = Auth::user()->id;

        if ($user_id) {
            $address = $request->address;
            $postal_code = $request->postal_code;
            DB::beginTransaction();

            $userAddress->address = $address;
            $userAddress->postal_code = $postal_code;
            $userAddress->user_id = $user_id;
            $userAddress->status = 1;
            $userAddress->created_at = date('Y-m-d');


            $add =  $userAddress->save();

            if ($add) {
                DB::commit();
                return response()->json(['message' => 'Alamat berhasil ditambahkan'], 200);
            } else {
                return response()->json(['message' => 'Gagal menambahkan alamat'], 400);
            }
        } else {
        }
    }

    public function detail(Request $request, $id)
    {

        $address = User_address::where('address_id', $id)->first();

        if ($address) {
            return response()->json($address, 200);
        } else {
            return response()->json(['message' => 'Alamat Kosong'], 204);
        }
    }

    public function update(Request $request)
    {
        $address_id = $request->address_id;
        $validator = Validator::make($request->all(), [
            'address' => 'required',
            'postal_code' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        DB::beginTransaction();
        $dataAddress = array(
            'address' => $request->address,
            'postal_code' => $request->postal_code
        );

        $update = User_address::where('address_id', $address_id)->update($dataAddress);
        if ($update) {
            DB::commit();
            return response()->json(['message' => 'Alamat berhasil di update'], 200);
        } else {
            DB::rollBack();
            return response()->json(['message' => 'Alamat gagal di update'], 400);
        }
    }
    public function delete(Request $request, $id)
    {
        $update = User_address::where('address_id', $id)->update(array('status' => 0));

        if ($update) {
            return response()->json(['message' => 'Alamat berhasil di hapus'], 200);
        } else {
            return response()->json(['message' => 'Alamat gagal di hapus'], 400);
        }
    }

    public function setPriority(Request $request, $id)
    {
        DB::beginTransaction();
        User_address::query()->update(array('priority' => 0));

        $priority = User_address::where('address_id', $id)->update(array('priority' => 1));

        if ($priority) {
            DB::commit();
            return response()->json(['message' => 'Alamat utama berhasil di atur'], 200);
        } else {
            DB::rollBack();
            return response()->json(['message' => 'Alamat utama gagal di atur'], 400);
        }
    }
}
