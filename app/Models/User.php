<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use HasApiTokens, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     * 
     */

    protected $table = 'users';
    protected $fillable = [
        'name',
        'role_id',
        'email',
        'password',
        'image'
    ];

    public function products()
    {
        return $this->hasMany(Products::class);
    }
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    static function getDataUser($id)
    {

        $data = User::select('*')
            ->where('id', '=', $id)
            ->first();
        return $data;
    }

    static function cekUserVerified($id)
    {

        $data = User::select('*')
            ->where('id', '=', $id)
            ->where('status_id', '=', 3)
            ->where('role_id', 2)
            ->first();
        return $data;
    }

    static function getDataUserByEmail($email)
    {

        $data = User::select('*')
            ->where('email', '=', $email)
            ->where('status_id', '=', 2)
            ->first();
        return $data;
    }

    // public function role()
    // {
    //     return $this->belongsTo('App\Models\User', 'role_id');
    //     // return $this->belongsToMany(User::class);
    // }

    public function hasRole($roles)
    {
        $this->have_role = $this->getUserRole();

        if (is_array($roles)) {
            foreach ($roles as $need_role) {
                if ($this->cekUserRole($need_role)) {
                    return true;
                }
            }
        } else {
            return $this->cekUserRole($roles);
        }
        return false;
    }
    private function getUserRole()
    {
        return  Auth::user()->role_id;
    }

    private function cekUserRole($role)
    {
        return (strtolower($role) == $this->getUserRole()) ? true : false;
    }
}
