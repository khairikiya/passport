<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Products extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'price'
    ];

    static function getDataProduk()
    {
        $sql = "SELECT * FROM products";
        $data = DB::select($sql);
        return $data;
    }
    static function getDataDetail($id)
    {
        $data = Products::select('*')
            ->where('product_id', '=', $id)
            ->first();
        return $data;
    }
}
