<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;



class Bank extends Model
{
    use HasFactory;

    static function getDataBankById($id)
    {
        $data = Bank::select('*')
            ->where('bank_id', '=', $id)
            ->first();
        return $data;
    }
}
