<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_address extends Model
{
    use HasFactory;

    static function getAlamatByUserId($id)
    {
        $data = User_address::select('*')
            ->where('user_id', $id)
            ->where('status', 1)
            ->get();
        return $data;
    }
}
