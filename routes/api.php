<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\BankController;
use App\Http\Controllers\PassportController;
use App\Http\Controllers\ProductsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [PassportController::class, 'login']);
Route::post('loginAdmin', [PassportController::class, 'loginAdmin']);
Route::post('register', [PassportController::class, 'register']);
Route::post('registerAdmin', [PassportController::class, 'registerAdmin']);
Route::post('konfirmasiOtp', [PassportController::class, 'konfirmasiOtp']);
Route::post('kirimOtp', [PassportController::class, 'kirimOtp']);

Route::group(['middleware' => ['auth:api', 'roles']], function () {
    Route::group(['prefix' => 'product'], function () {
        //GET DATA Product Key
        Route::get('/', [ProductsController::class, 'index']);
        Route::post('/', [ProductsController::class, 'store']);
        Route::get('/{id}', [ProductsController::class, 'detail']);
        Route::put('/{id}', [ProductsController::class, 'update']);
        Route::put('/delete/{id}', [ProductsController::class, 'delete']);
    });
    Route::group(['prefix' => 'bank'], function () {
        //GET DATA BANKS
        Route::post('/', [BankController::class, 'index']);
        Route::post('/add', [BankController::class, 'store']);
        Route::get('/{id}', [BankController::class, 'detail']);
        Route::put('/{id}', [BankController::class, 'update']);
        Route::put('/delete/{id}', [BankController::class, 'delete']);
    });
    Route::group(['prefix' => 'address', 'roles' => '2'], function () {
        //GET DATA address
        Route::get('/', [AddressController::class, 'index']);
        Route::post('/', [AddressController::class, 'store']);
        Route::get('/{id}', [AddressController::class, 'detail']);
        Route::put('/', [AddressController::class, 'update']);
        Route::put('/delete/{id}', [AddressController::class, 'delete']);
        Route::put('/setPriority/{id}', [AddressController::class, 'setPriority']);
    });
});

// Route::group(['middleware' => 'auth:api'], function () {
//     Route::get('user', [PassportController::class], 'details');
//     // Route::resource('product', [ProductController::class]);
// });

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
